var sanitizer = require('sanitizer');

const mapMongoUniqueError = (error) => {
    let field = error.message.split('.$')[1];
    field = field.split(' dup key')[0];
    field = field.substring(0, field.lastIndexOf('_'));
    return { [field]: [{ error: 'taken' }] };
  };

const mapMongoosErrors = (errors) => Object.keys(errors).reduce((agg, ele) => {
    agg[ele] = [{ error: errors[ele].message }];
    return agg;
}, {});


function sanitize(body) {
    Object.keys(body).map(element => {
        body[element] = sanitizer.sanitize(body[element])
    });
};


module.exports = {
    mapMongoosErrors,
    mapMongoUniqueError,
    sanitize
};