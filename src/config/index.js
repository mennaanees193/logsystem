const pubChannel = require('./publisher');
const subChannel = require('./subscriber');
const queue = "log";

module.exports = {
    pubChannel,
    subChannel,
    queue
};