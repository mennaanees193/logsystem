const express = require('express');
const router = express.Router();
const Log = require('../models/log');
const { pubChannel, subChannel, queue } = require('./../config');
const { sanitize } = require('../helpers');

async function sanitizeRequest(body) {
    sanitize(body);
    const channel = await pubChannel;
    channel.assertQueue(queue, {
        durable: true
    });
    channel.sendToQueue(queue, Buffer.from(JSON.stringify(body)), {
        persistent: true
    });

};

async function createLog() {

    const channel = await subChannel;

    channel.assertQueue(queue, {
        durable: true
    });

    const log = new Promise(function (resolve, reject) {
        channel.consume(queue, async function (msg) {
            const data = JSON.parse(msg.content);
            try {
                resolve(await Log.create({
                    title: data.title,
                    description: data.description,
                    statusCode: data.statusCode,
                    path: data.path,
                    createdAt: data.createdAt
                }))
            } catch (errors) {
                reject(errors)
            }

        }, {
                noAck: true
            });
    })
    return log

}
router.post('/', async (req, res) => {
    const { body } = req;
    try {
        sanitizeRequest(body);
        const logResult = await createLog();
        res
            .status(201)
            .send({ data: logResult });
    } catch (errors) {
        res
            .status(400)
            .send({ errors });
    }
});

router.get('/', async (req, res) => {
    try {
        const logs = await Log.find();
        res
            .status(201)
            .send({ data: logs });
    } catch (errors) {
        res
            .status(400)
            .send({ errors });
    }
});

router.get('/:id', async (req, res) => {
    const { params: { id } } = req;
    try {
        const log = await Log.findOne({ _id: id });
        res
            .status(201)
            .send({ data: log });
    } catch (errors) {
        res
            .status(400)
            .send({ errors });
    }
});
module.exports = router;
