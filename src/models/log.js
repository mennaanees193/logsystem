const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { mapMongoosErrors } = require('../helpers');

const logSchema = new Schema({
  title: {
    type: String,
    required: [true, 'title is Required']
  },
  description: {
    type: String,
    required: [true, 'description is Required']
  },
  statusCode: {
    type: String,
    required: [true, 'statusCode is Required']
  },
  path: {
    type: String,
    required: [true, 'path is Required'],
    index: true
  },
  createdAt: {
    type: Date
  },
});

logSchema.post('save', (error, doc, next) => {
  if (!error) next();
  return next(mapMongoosErrors(error.errors));
});

mongoose.set('useCreateIndex', true);
const Log = mongoose.model('log', logSchema);
module.exports = Log;