var amqp = require('amqplib/callback_api');

var subChannel = new Promise(function (resolve, reject) {
    amqp.connect(process.env.RMQ_URL, function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }
            resolve(channel)

        });
    });
})
module.exports = subChannel;
